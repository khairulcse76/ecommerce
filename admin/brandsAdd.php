﻿<?php 
    session_start();
    include 'inc/header.php';
    include 'inc/sidebar.php';
    include_once '../vendor/autoload.php';
    use ecommerce\brand;
    
    
    
    $catagoryobj=new brand();
    
    if($_SERVER['REQUEST_METHOD']=='POST')
    {
        $brandname=$_POST['brandname'];
        if(empty($brandname))
        {
             $_SESSION['insert_msg']='<b style=" color:blue; font-size: 16px; ">field must be not empty..?';
        }  else {
            $catagoryobj->prepare($_POST)->Insertbrands();
        }
        
    }
    
//    print_r($_POST);
    
    ?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>New Brand</h2>
               <div class="block copyblock"> 
                   <form action="" method="POST">
                    <table class="form">
                        <?php 
                            if(isset( $_SESSION['insert_msg']))
                            { ?>
                        <tr>
                            <td>
                                <?php 
                                    echo  $_SESSION['insert_msg'];
                                    unset( $_SESSION['insert_msg']);
                                ?>
                            </td>
                        </tr>
                           <?php }
                        ?>
                        <tr>
                            <td>
                                <input type="text" name="brandname" placeholder="Enter Brands Name..." class="medium" />
                                
                            </td>
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
<?php include 'inc/footer.php';
    
?>