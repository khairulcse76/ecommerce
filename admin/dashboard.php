﻿<?php 
    include_once '../vendor/autoload.php';
    use ecommerce\AdminLogin;
    $adminlogin=new AdminLogin();
    if(isset($_SESSION['user']))
    {
include 'inc/header.php';
include 'inc/sidebar.php'; 
  ?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2> Dashbord</h2>
                <div class="block">
                    <?php if(isset( $_SESSION['login_msg']))
                    { ?>
                    <p>
                        <?php
                        echo  $_SESSION['login_msg'];
                        unset($_SESSION['login_msg']);
                        
                        ?>
                    </p>
                   <?php }
                        ?>
                    </div>
            </div>
        </div>
<?php include 'inc/footer.php';
    } else {
        $_SESSION['emty_msg']="Login first";
        header('location:login.php');
    }
?>