﻿<?php 
    include 'inc/header.php';
    include 'inc/sidebar.php';
    include_once '../vendor/autoload.php';
    use ecommerce\catagory;
    
    
    
    $catagoryobj=new catagory();
    
    if($_SERVER['REQUEST_METHOD']=='POST')
    {
        $catagoryName=$_POST['catname'];
        if(empty($catagoryName))
        {
             $_SESSION['insert_msg']='<b style=" color:blue; font-size: 16px; ">field must be not empty..?';
        }  else {
            $catagoryobj->prepare($_POST)->catagoryInsert();
        }
        
    }
    
//    print_r($_POST);
    
    ?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock"> 
                   <form action="" method="POST">
                    <table class="form">
                        <?php 
                            if(isset( $_SESSION['insert_msg']))
                            { ?>
                        <tr>
                            <td>
                                <?php 
                                    echo  $_SESSION['insert_msg'];
                                    unset( $_SESSION['insert_msg']);
                                ?>
                            </td>
                        </tr>
                           <?php }
                        ?>
                        <tr>
                            <td>
                                <input type="text" name="catname" placeholder="Enter Category Name..." class="medium" />
                            </td>
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
<?php include 'inc/footer.php';
    
?>