﻿<?php 
    session_start();
    include 'inc/header.php';
    include 'inc/sidebar.php';
    
    include_once '../vendor/autoload.php';
    use ecommerce\brand;
    use ecommerce\catagory;
    use ecommerce\product;
    
    
     
    if($_SERVER['REQUEST_METHOD']=='POST')
    {
            $permited  = array('jpg', 'jpeg', 'png', 'gif');
            $file_name = $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_temp = $_FILES['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
            $uploaded_image = "uploads/".$unique_image;
    
        if(empty($_POST['productName']))
        {
             $_SESSION['emt_msg']='<b style=" color:red; font-size: 16px; ">* filed requer..!!!</b>';
        }  else {
            if($_POST['catagoryId']=='none')
            {
                 $_SESSION['ctemt_msg']='<b style=" color:red; font-size: 16px; ">* filed requer..!!!</b>';
            }else {
            if($_POST['brandId']=='none')
            {
              $_SESSION['brndemt_msg']='<b style=" color:red; font-size: 16px; ">* filed requer..!!!</b>';
            }  else {
                 if(empty($_POST['price']))
                 {
                   $_SESSION['priceemt_msg']='<b style=" color:red; font-size: 16px; ">* filed requer..!!!</b>';
                 }  else {
                    {
                        move_uploaded_file($file_temp, $uploaded_image);
                        $productobj=new product();
                        $productobj->prepare($_POST)->Insertproduct();
                 }
            }
         }
       }
        
    }
  }
    
    
    ?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Product</h2>
        <div class="block">               
         <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
               <?php if(isset($_SESSION['insert_msg'])){?><p><?php echo $_SESSION['insert_msg']; unset($_SESSION['insert_msg']); ?></p> <?php } ?>
                <tr>
                    <td>
                        <label>Name<span style="color: red;">*</span></label>
                    </td>
                    <td>
                        <input type="text" name="productName" placeholder="Enter Product Name..." class="medium" />
                        <label><?php if(isset( $_SESSION['emt_msg'])){ echo  $_SESSION['emt_msg']; unset( $_SESSION['emt_msg']);} ?></label>
                    </td>
                </tr>
		<tr>
                    <td>
                        <label>Category<span style="color: red;">*</span></label>
                    </td>
                    <td>
                        <select id="select" name="catagoryId">
                            <option value="none">Select Category</option>
                            <?php 
                                $object=new catagory();
                               $data= $object->cataview();
                                foreach ($data as $value)
                                { ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['catName'] ?></option>
                               <?php }
                            ?>
                        </select>
                         <label><?php if(isset( $_SESSION['ctemt_msg'])){ echo  $_SESSION['ctemt_msg']; unset( $_SESSION['ctemt_msg']);} ?></label>
                    </td>
                </tr>
		<tr>
                    <td>
                        <label>Brand<span style="color: red;">*</span></label>
                    </td>
                    <td>
                        <select id="select" name="brandId">
                            <option value="none">Select Brand</option>
                            <?php 
                                $objectb=new brand();
                               $datab= $objectb->brandsview();
                                foreach ($datab as $value)
                                { ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['BrandsName'] ?></option>
                               <?php }
                            ?>
                           
                        </select> <label><?php if(isset( $_SESSION['brndemt_msg'])){ echo  $_SESSION['brndemt_msg']; unset( $_SESSION['brndemt_msg']);} ?></label>
                    </td>
                </tr>
		<tr>
                    <td>
                        <label>Price<span style="color: red;">*</span></label>
                    </td>
                    <td>
                        <input type="text" name="price" placeholder="Enter Price..." class="medium" />
                        <label><?php if(isset( $_SESSION['priceemt_msg'])){ echo  $_SESSION['priceemt_msg']; unset( $_SESSION['priceemt_msg']);} ?></label>
                    </td>
                </tr>
            
                <tr>
                    <td>
                        <label>Upload Image<span style="color: red;">*</span></label>
                    </td>
                    <td>
                        <input type="file" name="image" />
                        <label><?php if(isset( $_SESSION['img_emp_error'])){ echo  $_SESSION['img_emp_error']; unset( $_SESSION['img_emp_error']);} ?></label>
                    </td>
                </tr>
				
		<tr>
                    <td>
                        <label>Product Type</label>
                    </td>
                    <td>
                        <select id="select" name="type">
                            <option>Select Type</option>
                            <option value="1">Featured</option>
                            <option value="2">Non-Featured</option>
                        </select>
                    </td>
                </tr>
                   <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Description</label>
                    </td>
                    <td>
                        <textarea class="tinymce" name="description"></textarea>
                    </td>
                </tr>
                
		<tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Save" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>

<!-- Load TinyMCE -->
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include 'inc/footer.php';?>


