<?php 
//session_start();
    include_once '../vendor/autoload.php';
    use ecommerce\AdminLogin;
    $adminlogin=new AdminLogin();
    if(isset($_SESSION['user']))
{
    $_SESSION['login_msg']='<span style="color: red; font-size:36px; ">please logout first</span>';
     header('location:dashboard.php');
} else{
  if($_SERVER['REQUEST_METHOD']=='POST')
  {
   $username=$_POST['username'];
   $password=$_POST['password'];
   
   if(empty($username) || empty($password))
   {
       $_SESSION['err_msg']= "Field must be not empty..!";
   }  else {
       $loginchk=$adminlogin->prepare($_POST)->login();
       
       if($loginchk)
       {
           $_SESSION['login_msg']='<b style=" color:blue; font-size: 16px; ">Login Successfull <br>Welcome control penel.';
           header('location:dashboard.php');
       }  else {
       
           $_SESSION['err_msg']='<b style=" color:blue; font-size: 16px; ">user name or password dose not match..!!';
           header('location:login.php');
       }
    }
  }
?>



<head>
<meta charset="utf-8">
<title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
<div class="container">
	<section id="content">
		<form action="" method="post">
			<h1>Admin Login</h1>
                        <?php
                            if(isset( $_SESSION['err_msg']))
                            { ?>
                        <p style="color: red; font-size: 16px;"><?php 
                        echo $_SESSION['err_msg'];
                         unset($_SESSION['err_msg']);
                        ?></p>
                           <?php }
                        ?>
                            
			<div>
				<input type="text" placeholder="Username"  name="username"/>
			</div>
			<div>
				<input type="password" placeholder="Password"  name="password"/>
			</div>
			<div>
				<input type="submit" value="Log in" />
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="#">OOP based website </a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
</body>
</html>

<?php } ?>