<?php
namespace ecommerce;
//session_start();
//use ecommerce\Database;
//use ecommerce\Helper\Format;
use PDO;

class catagory {
    
    public $user='root';
    public $password='';
    public $connection='';
    public $error='';
    public $catagory='';
    public $unique_id='';




    public function __construct(){
        
         $this->connection = new PDO('mysql:host=localhost;dbname=e_commerce', $this->user, $this->password);
         
    }
 
 
    public function prepare($data='')
    {
//        print_r($data);
        if(array_key_exists('catname', $data))
        {
            $this->catagory=$data['catname'];
        }
        if(array_key_exists('unique_id', $data))
        {
            $this->unique_id=$data['unique_id'];
        }
       
        return $this;
    }
    
    public function catagoryInsert()
    {
//        echo $this->catagory;
//        die();
        
         $q = "INSERT INTO  catagory_tbl(id, catName, unique_id)
                VALUES(:ide, :cat, :unique_id)";
               $stmt=  $this->connection->prepare($q);
               $result=$stmt->execute(array(
                ':ide' => Null,
                'unique_id' => uniqid(),
                ':cat' => $this->catagory,
                   ));
               $_SESSION['insert_msg']='<b style=" color: blue;">Catagory Added</b>';
    }
    
    
    public function cataview()
    {
       $qry="SELECT * FROM catagory_tbl";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
     public function show()
    {
        $qry="SELECT * FROM catagory_tbl WHERE unique_id="."'".$this->unique_id."'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $row=$stmt->fetch();
       return $row;
    }
    
    
     public function cataupdate()
    {
        try {
       
              $sql = "UPDATE catagory_tbl SET catName='$this->catagory' WHERE unique_id="."'".$this->unique_id."'";
               
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['store_msg']='<b style=" color: green;">Data Update Successful</b>';
                header("location:catedit.php?unique_id=$this->unique_id");
                }
        } catch (Exception $e) {
            
        }
    }
    
     public function delete()
    {
        try {
              $query = "DELETE FROM `catagory_tbl` WHERE `catagory_tbl`.`unique_id` =" . "'" . $this->unique_id . "'";
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                    $_SESSION['delete_msg']='<b style=" color: red;">Data Delete Successful</b>';
                    header("location:catlist.php");
                }    
        } catch (Exception $ex) {
            
        }
    }
   
}
