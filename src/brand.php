<?php

namespace ecommerce;
use PDO;

class brand {
    
    public $user='root';
    public $password='';
    public $connection='';
    public $error='';
    public $brandname='';
    public $unique_id='';




    public function __construct(){
        
         $this->connection = new PDO('mysql:host=localhost;dbname=e_commerce', $this->user, $this->password);
         
    }
 
 
    public function prepare($data='')
    {
//        print_r($data);
        if(array_key_exists('brandname', $data))
        {
            $this->brandname=$data['brandname'];
        }
        if(array_key_exists('unique_id', $data))
        {
            $this->unique_id=$data['unique_id'];
        }
       
        return $this;
    }
    
    public function Insertbrands()
    {
//        echo $this->catagory;
//        die();
//        INSERT INTO `` (`id`, `unique_id`, `BrandsName`, `is_active`) VALUES (NULL, 'dfgfgdfgdf', 'sdfgdf', '0');

         $q = "INSERT INTO  brands_tbl(id, unique_id, BrandsName, is_active)
                VALUES(:ide, :unique_id, :brdn, :is_active)";
               $stmt=  $this->connection->prepare($q);
               $result=$stmt->execute(array(
                ':ide' => Null,
                'unique_id' => uniqid(),
                ':brdn' => $this->brandname,
                ':is_active'=>0,
                   ));
               $_SESSION['insert_msg']='<b style=" color: blue;">Brand Inserted</b>';
    }
    
    
    public function brandsview()
    {
       $qry="SELECT * FROM brands_tbl";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
     public function show()
    {
        $qry="SELECT * FROM brands_tbl WHERE unique_id="."'".$this->unique_id."'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $row=$stmt->fetch();
       return $row;
    }
    
    
     public function brandupdate()
    {
        try {
       
              $sql = "UPDATE brands_tbl SET BrandsName='$this->brandname' WHERE unique_id="."'".$this->unique_id."'";
               
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['store_msg']='<b style=" color: green;">Data Update Successful</b>';
                header("location:brandsedit.php?unique_id=$this->unique_id");
                }
        } catch (Exception $e) {
            
        }
    }
    
     public function branddelete()
    {
        try {
              $query = "DELETE FROM `brands_tbl` WHERE `brands_tbl`.`unique_id` =" . "'" . $this->unique_id . "'";
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                $_SESSION['delete_msg']='<b style=" color: red;">Data Delete Successful</b>';
                header("location:brandslist.php");
                }    
        } catch (Exception $ex) {
            
        }
    }
   
}


