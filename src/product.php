<?php
namespace ecommerce;

use PDO;

class product {
   public $id='';
   public $user='root';
   public $password='';
   public $connection='';
   public $error='';
   public $unique_id='';
   private $productName='';
   private $catagoryId='';
   private $brandId='';
   private $price='';
   private $image='';
   private $type='';
   public $description='';




    public function __construct(){
        
         $this->connection = new PDO('mysql:host=localhost;dbname=e_commerce', $this->user, $this->password);
         date_default_timezone_set('Asia/Dhaka');
         
    }
 
 
    public function prepare($data='')
    {
//        print_r($data);
        if(array_key_exists('productName', $data))
        {
            $this->productName=$data['productName'];
        }
        if(array_key_exists('catagoryId', $data))
        {
            $this->catagoryId=$data['catagoryId'];
        }
        if(array_key_exists('brandId', $data))
        {
            $this->brandId=$data['brandId'];
        }
        if(array_key_exists('price', $data))
        {
            $this->price=$data['price'];
        }
        if(array_key_exists('image', $data))
        {
            $this->image=$data['image'];
        }
        if(array_key_exists('type', $data))
        {
            $this->type=$data['type'];
        }
        if(array_key_exists('description', $data))
        {
            $this->description=$data['description'];
        }
         if(array_key_exists('id', $data))
        {
            $this->id=$data['id'];
        }
        
         if(array_key_exists('unique_id', $data))
        {
            $this->unique_id=$data['unique_id'];
        }
        
         if(array_key_exists('productName', $data))
        {
            $this->productName=$data['productName'];
        }
       
        return $this;
    }
    
    public function Insertproduct()
    {
//        echo date('d/m/Y h:i:s A');
//        die();
//        INSERT INTO `` (`id`, `unique_id`, `BrandsName`, `is_active`) VALUES (NULL, 'dfgfgdfgdf', 'sdfgdf', '0');

         $query = "INSERT INTO  products_tbl
                        (id, unique_id, productName, catagoryId, brandId, description, price, image, type, created, modified, deleted)
                VALUES(:ide, :unique_id, :productName, :catagoryId, :brandId, :description, :price, :image, :type, :created, :modified, :deleted)";
               $stmt=  $this->connection->prepare($query);
               $result=$stmt->execute(array(
                ':ide' => Null,
                'unique_id' => uniqid(),
                ':productName' => $this->productName,
                ':catagoryId'=> $this->catagoryId,
                ':brandId' => $this->brandId,
                'description' => $this->description,
                ':price' => $this->price,
                ':image'=> $this->image,
                ':type'=> $this->type,
                ':created' => date('d/m/Y h:i:s A'),
                'modified' => date('d/m/Y h:i:s A'),
                ':deleted' => date('d/m/Y h:i:s A'),
                
                   ));
               $_SESSION['insert_msg']='<b style=" color: blue;">Product Inserted</b>';
    }
    
    
    public function productview()
    {
        
       $query="SELECT products_tbl.*, catagory_tbl.catName, brands_tbl.BrandsName
           FROM products_tbl
           INNER JOIN catagory_tbl
           ON catagory_tbl.id = products_tbl.catagoryId
           INNER JOIN brands_tbl
           ON brands_tbl.id = products_tbl.brandId
           ORDER BY products_tbl.id DESC";
       $stmt=  $this->connection->query($query);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
     public function show()
    {
        $qry="SELECT * FROM products_tbl WHERE unique_id="."'".$this->unique_id."'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll(PDO::FETCH_ASSOC);
       return $table;
    }
    
    
     public function brandupdate()
    {
        try {
       
              $sql = "UPDATE brands_tbl SET BrandsName='$this->brandname' WHERE unique_id="."'".$this->unique_id."'";
               
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['store_msg']='<b style=" color: green;">Data Update Successful</b>';
                header("location:brandsedit.php?unique_id=$this->unique_id");
                }
        } catch (Exception $e) {
            
        }
    }
    
     public function branddelete()
    {
        try {
              $query = "DELETE FROM `brands_tbl` WHERE `brands_tbl`.`unique_id` =" . "'" . $this->unique_id . "'";
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                $_SESSION['delete_msg']='<b style=" color: red;">Data Delete Successful</b>';
                header("location:brandslist.php");
                }    
        } catch (Exception $ex) {
            
        }
    }
}
