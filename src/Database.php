<?php
namespace ecommerce;
use PDO;
Class Database{
 
    public $user='root';
    public $password='';
    public $connection='';
    public $error='';
 
 public function __construct(){
  $this->connection = new PDO('mysql:host=localhost;dbname=e_commerce', $this->user, $this->password);
 }
 
 public function connectDB(){
    $this->connection = new PDO('mysql:host=localhost;dbname=e_commerce', $this->user, $this->password);
    if(!$this->connection){
      $this->error ="Connection fail".$this->connection->connect_error;
     return false;
    }
    return $this;
 }
 
// Select or Read data
public function select($query){
    $result = $this->connection->query($query);
    $result->execute();
    if($result->rowCount()> 0){
      return $result;
    } else {
      return false;
    }
 }
 
// Insert data
public function insert($query){
    $insert_row = $this->connection->query($query) or die($this->connection->error.__LINE__);
    if($insert_row){
      return $insert_row;
    } else {
      return false;
     }
 }
 
// Update data
 public function update($query){
    $update_row = $this->connection->query($query) or die($this->connection->error.__LINE__);
    if($update_row){
     return $update_row;
    } else {
     return false;
     }
 }
 
// Delete data
 public function delete($query){
    $delete_row = $this->connection->query($query) or die($this->connection->error.__LINE__);
    if($delete_row){
      return $delete_row;
    } else {
      return false;
     }
    }
 
}