-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2016 at 05:26 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `khairul`
--

-- --------------------------------------------------------

--
-- Table structure for table `catagory_tbl`
--

CREATE TABLE `catagory_tbl` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(256) NOT NULL,
  `catName` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catagory_tbl`
--

INSERT INTO `catagory_tbl` (`id`, `unique_id`, `catName`) VALUES
(3, 'sdfgr566hj66', 'Mobile Phones'),
(4, 'dfl444gk954', 'Desktop'),
(5, '5s4dfdg65f4', 'Laptop'),
(6, '4dfs454sdsdf', 'Accessories'),
(7, '6df5gfd65g', 'Accessories'),
(8, 'dfg5fg4df65gd', 'Software'),
(9, 'sdf5g4sd5fg46f', 'Sports & Fitness'),
(10, 'df54gsd5fg4', 'Footwear'),
(11, 'd5fg4sd54fgdf4', 'Clothing'),
(12, 'sdfg65fgdsd6fg', 'Home Decor & Kitchen'),
(13, '5g4sd6fsd2fg5', 'Beauty & Healthcare'),
(14, 'sd6fg5dfgd5fg', 'Toys, Kids & Babies'),
(15, 'sdf45sdfg65fg65f', 'bangladesh'),
(16, '57a0baf4268d1', 'new');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catagory_tbl`
--
ALTER TABLE `catagory_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catagory_tbl`
--
ALTER TABLE `catagory_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
