-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2016 at 11:59 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `products_tbl`
--

CREATE TABLE `products_tbl` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(256) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `catagoryId` int(11) NOT NULL,
  `brandId` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` float(10,3) NOT NULL,
  `image` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` varchar(100) NOT NULL,
  `modified` varchar(1000) NOT NULL,
  `deleted` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_tbl`
--

INSERT INTO `products_tbl` (`id`, `unique_id`, `productName`, `catagoryId`, `brandId`, `description`, `price`, `image`, `type`, `created`, `modified`, `deleted`) VALUES
(1, '57a1abcc77aab', 'Bangladesh', 3, 1, '<p>Nothing</p>', 1600.000, '', '1', '03/08/2016 02:31:08 PM', '03/08/2016 02:31:08 PM', '03/08/2016 02:31:08 PM'),
(2, '57a1acba25cd8', 'MaxBag', 16, 1, '<p>Nothing</p>', 1600.000, '', '2', '03/08/2016 02:35:06 PM', '03/08/2016 02:35:06 PM', '03/08/2016 02:35:06 PM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products_tbl`
--
ALTER TABLE `products_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products_tbl`
--
ALTER TABLE `products_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
